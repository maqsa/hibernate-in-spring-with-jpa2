package work;

import java.util.List;

public interface ContactSummaryService {
    List<ContactSummary> findAll();
}
