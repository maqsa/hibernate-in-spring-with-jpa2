package work;

import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.List;

public class SpringJPASampleSummary {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:META-INF/spring/app-context-annotation.xml");
        ctx.refresh();

        ContactSummaryUntypeImpl contactSummaryUntype = ctx.getBean(
                "contactSummaryUntype", ContactSummaryUntypeImpl.class);
        contactSummaryUntype.displayAllContactSummary();
    }

}
