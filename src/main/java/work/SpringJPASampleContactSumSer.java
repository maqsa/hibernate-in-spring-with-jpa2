package work;

import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.List;

public class SpringJPASampleContactSumSer {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:META-INF/spring/app-context-annotation.xml");
        ctx.refresh();

        ContactSummaryService contactSummaryService = ctx.getBean(
                "contactSummaryService", ContactSummaryService.class);
        List<ContactSummary> contacts = contactSummaryService.findAll();

        for (ContactSummary contactSummary: contacts){
            System.out.println(contactSummary);
        }
    }

}
