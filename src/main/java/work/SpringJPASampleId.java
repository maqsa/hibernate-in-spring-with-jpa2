package work;

import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.List;

public class SpringJPASampleId {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:META-INF/spring/app-context-annotation.xml");
        ctx.refresh();

        ContactService contactService = ctx.getBean(
                "jpaContactService", ContactService.class);
        Contact contact = contactService.findById(1l);
        System.out.println("");
        System.out.println("Contact with id 1:" + contact);
        System.out.println("");
    }

}
