package work;

import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.Date;
import java.util.List;

public class SpringJPASampleInsert {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:META-INF/spring/app-context-annotation.xml");
        ctx.refresh();

        ContactService contactService = ctx.getBean(
                "jpaContactService", ContactService.class);

        Contact contact = new Contact();
        contact.setFirstName("Micky");
        contact.setLastName("Jayson");
        contact.setBirthDate(new Date());

        ContactTelDetail contactTelDetail = new ContactTelDetail("Home", "111111");

        contact.addContactTelDetail(contactTelDetail);

        contactService.save(contact);

        listContactsWithDetail(contactService.findAllWithDetail());
    }

    private static void listContactsWithDetail(List<Contact> contacts) {
        System.out.println("");
        System.out.println("Listing contacts without details:");
        for (Contact contact: contacts){
            System.out.println(contact);
            if (contact.getContactTelDetails() != null){
                for (ContactTelDetail contactTelDetail: contact.getContactTelDetails()){
                    System.out.println(contactTelDetail);
                }
            }
            if (contact.getHobbies() != null){
                for (Hobby hobby: contact.getHobbies()){
                    System.out.println(hobby);
                }
            }
            System.out.println();
        }
    }
}
