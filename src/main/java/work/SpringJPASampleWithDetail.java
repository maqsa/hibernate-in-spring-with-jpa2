package work;

import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.List;

public class SpringJPASampleWithDetail {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:META-INF/spring/app-context-annotation.xml");
        ctx.refresh();

        ContactService contactService = ctx.getBean(
                "jpaContactService", ContactService.class);

        List<Contact> contacts = contactService.findAllWithDetail();
        listContactsWithDetail(contacts);
    }

    private static void listContactsWithDetail(List<Contact> contacts) {
        System.out.println("");
        System.out.println("Listing contacts without details:");
        for (Contact contact: contacts){
            System.out.println(contact);
            if (contact.getContactTelDetails() != null){
                for (ContactTelDetail contactTelDetail: contact.getContactTelDetails()){
                    System.out.println(contactTelDetail);
                }
            }
            if (contact.getHobbies() != null){
                for (Hobby hobby: contact.getHobbies()){
                    System.out.println(hobby);
                }
            }
            System.out.println();
        }
    }
}
